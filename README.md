# delightful sustainable business [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of sustainable business resources and exemplars of online businesses.

## Contents

- [Introduction](#introduction)
- [Exemplars](#exemplars)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Introduction

Wikipedia defines a [Sustainable business](https://en.wikipedia.org/wiki/Sustainable_business) but puts most emphasis on 'green' aspects. This list is interested in holistic aspects of sustainability along the lines of the [Circles of sustainability](https://en.wikipedia.org/wiki/Circles_of_Sustainability) method. The relevant parts of the definition then are:

> A sustainable business is an enterprise that has minimal negative impact, or potentially a positive effect, on the global or local environment, community, society, or economy — a business that strives to meet the [triple bottom line](https://en.wikipedia.org/wiki/Triple_bottom_line). It incorporates principles of sustainability into each of its business decisions.

Furthermore this lists focuses _exclusively_ on **resources for online businesses** i.e. those companies that offer FOSS-based - free and open-source software - products, and SaaS-based - software-as-a-service - service providers, or other online services.

## Exemplars

An alphabetically sorted list of known sustainable businesses that truly stand out (candidates will be vetted, see [#1](https://codeberg.org/teaserbot-labs/delightful-sustainable-business/issues/1)).

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/teaserbot-labs/delightful-linked-data/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)